<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Objetos Rick y Morty</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="fontawesome/css2/all.css" />
<link type="text/css" rel="stylesheet" href="micss.css" />
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<body class="body2">
<br>
<div class="colocador">
    
          <img src="jerry.png" class="imagenesjuego">
         
</div>
<div class="cajaimgfondo"><img class="imgfondos" src="img/r1.jpg" /></div>
<div class="cajapreguntas">
	<h1>A quién pertenece este objeto</h1>
	
    
    <table class="tablarespuestas" align="center">
	  <tr>
	    <td><div class="example"><div class="boton_fondo_corredizo_base boton_fondo_corredizo_diagonal1">Respuesta 1</div></div></td>
	    <td><div class="example"><div class="boton_fondo_corredizo_base boton_fondo_corredizo_diagonal2">Respuesta 2</div></div></td>
      </tr>
	  <tr>
	    <td><div class="example"><div class="boton_fondo_corredizo_base boton_fondo_corredizo_diagonal3">Respuesta 3</div></div></td>
	    <td><div class="example"><div class="boton_fondo_corredizo_base boton_fondo_corredizo_diagonal4">Respuesta 4</div></div></td>
      </tr>
  </table>
 
</div>
</body>
</html>