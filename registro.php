<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registro</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="fontawesome/css2/all.css" />
<link type="text/css" rel="stylesheet" href="prubar.css" />
</head>

<body class="body1">
<div class="cajaregistro">

    <form action="Login.php" >
      <div class="form-group">
      	 <label for="exampleInputEmail1">Correo</label>
         <input type="email" class=" inputloquete" id="exampleInputEmail1" aria-describedby="emailHelp">
         <small id="emailHelp" class="form-text text-muted">Rellenalo correctamente tal que: ___@___.___</small>
      </div>
      <br>
      <div class="form-group">
      	 <label for="exampleInputEmail1">Usuario</label> &nbsp <i class="far fa-user"></i>
         <input type="text" placeholder="Introduce tu nombre de usuario" class="inputloquete" id="tTextoNombre" aria-describedby="emailHelp">
      </div>
      <br>
      <div class="form-group">
         <label for="exampleInputPassword1">Contraseña</label> &nbsp <i class="fas fa-lock"></i>
         <input type="password" class="inputloquete" placeholder="Introduce una contraseña" id="exampleInputPassword1">
      </div>
      <br>
      <div class="form-group">
         <label for="exampleInputPassword1">Repetir Contraseña</label> &nbsp <i class="fas fa-lock"></i>
         <input type="password" class="inputloquete" placeholder="Confirmar contraseña" id="exampleInputPassword1">
      </div>
      <br>
      <div class="colocador">
      	<button type="submit" class=" botonfantasmar">Registrarse</button>
      </div>
    </form>
</div>

</body>
</html>